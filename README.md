### 说明
目前主要是项目自用，适用范围比较单一，也可能会存在bug。这些情况之后会通过迭代慢慢改善的。

### Usage
```javascript
const formatJSON = require('/path/to/formatJSON.js')
const src = '{a:1,b:2}'
const result = formatJSON(src)
console.log(result)
/*
{
  a: 1,
  b: 2
}
 */
```