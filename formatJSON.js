const getTab = (tabCount) => {
  let tab = ''
  for (let i = 0; i < tabCount; i++) {
    tab += '  '
  }
  return tab
}

module.exports = (src) => {
  let map = {}
  let strCount = 0
  let tabCount = 0
  let result = src
    .replace(/\\'/g, '@@a')
    .replace(/\\"/g, '@@b')
    .replace(/"[^"]*"/g, (str) => {
      strCount += 1
      const key = '@@str' + strCount
      map[key] = str
      return key
    })
    .replace(/\s*/g, '')
    .replace(/:/g, ': ')

  const reg = new RegExp('\\{|\\}|\\[|\\]|,', 'g')
  let matched
  let copy = ''
  let begin = -1

  while ((matched = reg.exec(result)) !== null) {
    // console.log(`Found ${str}. Next starts at ${reg.lastIndex}.`)
    const str = matched[0]
    const tabs = reg.lastIndex - begin === 1 && str === ',' ? '' : getTab(tabCount)
    copy += tabs + result.substring(begin, reg.lastIndex - 1)

    if (str === '{' || str === '[') {
      tabCount += 1
      copy += str + '\n'
    } else if (str === ']' || str === '}') {
      tabCount -= 1
      copy += '\n' + getTab(tabCount) + str
    } else {
      copy += ',\n'
    }

    begin = reg.lastIndex
  }

  copy += result.substring(begin)

  // 开始还原
  for (let key in map) {
    copy = copy.replace(key, map[key])
  }

  copy = copy.replace(/@@a/g, "\\'")
  copy = copy.replace(/@@b/g, '\\"')

  return copy
}